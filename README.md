### List of AI Chatbots

- [Broken Bear](https://brokenbear.com) - An AI Teddy Bear that users can [vent to for free, online, and anonymously](www.brokenbear.com).
- [ChatGPT](https://chat.openai.com/chat) - ChatGPT by OpenAI is a large language model that interacts in a conversational way.
- [Bard](https://bard.google.com) - An experimental AI chatbot by Google, powered by the LaMDA model.
- [Character.AI](https://character.ai/) - Character.AI lets you create characters and chat to them.
- [ChatPDF](https://www.chatpdf.com/) - Chat with any PDF.
creation.
